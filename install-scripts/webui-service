#!/bin/bash

## Used on systems running the init process manager
init_install() {
    # Create an init script
    cat >/etc/init.d/lightshowpi-web <<EOF
#!/bin/sh

### BEGIN INIT INFO
# Provides:          lightshowpi-web
# Required-Start:    \$remote_fs \$syslog
# Required-Stop:     \$remote_fs \$syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: LightshowPi web service
# Description:       Manages the LightshowPi web server
### END INIT INFO

# Author: Kevin Lustic (klustic@gmail.com)
# Modified from /etc/init.d/skeleton

# Do NOT "set -e"

PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="LightshowPi web server"
NAME=lightshowpi_daemon.py
DAEMON=$SYNCHRONIZED_LIGHTS_HOME/py/\$NAME
SCRIPTNAME=/etc/init.d/lightshowpi-web

# Exit if the package is not installed
[ -x "\$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/\$NAME ] && . /etc/default/\$NAME

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.2-14) to ensure that this file is present
# and status_of_proc is working.
. /lib/lsb/init-functions

do_start()
{
    \$DAEMON start
    return \$?
}

do_stop()
{
    \$DAEMON stop
    return \$?
}

case "\$1" in
  start)
	echo "Starting \$DESC:" "\$NAME"
	do_start
	case "\$?" in
		0) echo Success ;;
		*) echo Service start failed ;;
	esac
	;;
  stop)
	echo "Stopping \$DESC:" "\$NAME"
	do_stop
	case "\$?" in
		0) echo Success ;;
		*) echo Failed to stop the service ;;
	esac
	;;
  restart)
    do_stop
    do_start
    ;;
  *)
	echo "Usage: \$SCRIPTNAME {start|stop|restart}" >&2
	exit 3
	;;
esac
EOF
    # Make the script executable
    chmod 755 /etc/init.d/lightshowpi-web

    # Install the script at startup
    update-rc.d lightshowpi-web defaults

    # Start the service
    service lightshowpi-web start

    # Print some usage
    echo ""
    echo "The lightshowpi web server is running and will start when your Pi boots"
    echo "Run the following commands to manage your server manually:"
    echo "  [root]# service lightshowpi-web start"
    echo "  [root]# service lightshowpi-web stop"
    echo "  [root]# service lightshowpi-web restart"
    echo ""
    echo "To manually disable/enable the server starting on bootup, respectively:"
    echo "  [root]# update-rc.d -f lightshowpi-web remove"
    echo "  [root]# update-rc.d lightshowpi-web defaults"
}  ## end of init installer


## Used on systems running the systemd process manager
systemd_install() {
    cat >/etc/systemd/system/lightshowpi-web.service <<EOF
# Author: Kevin Lustic (klustic@gmail.com)
[Unit]
Description=LightshowPi Web Server

[Service]
Type=forking
RemainAfterExit=yes
Environment="SYNCHRONIZED_LIGHTS_HOME=$SYNCHRONIZED_LIGHTS_HOME"
ExecStart=$SYNCHRONIZED_LIGHTS_HOME/py/lightshowpi_daemon.py start
ExecStop=$SYNCHRONIZED_LIGHTS_HOME/py/lightshowpi_daemon.py stop

[Install]
WantedBy=multi-user.target
EOF

    # Enable the service at startup
    systemctl enable lightshowpi-web.service

    # Start the service
    systemctl start lightshowpi-web.service

    # Print some usage
    echo ""
    echo "The lightshowpi web server is running and will start when your Pi boots"
    echo "Run the following commands to manage your server manually:"
    echo "  [root]# service lightshowpi-web start  -or-  systemctl start lightshowpi-web.service"
    echo "  [root]# service lightshowpi-web stop   -or-  systemctl stop lightshowpi-web.service"
    echo ""
    echo "To manually disable/enable the server starting on bootup, respectively:"
    echo "  [root]# systemctl disable lightshowpi-web.service"
    echo "  [root]# systemctl enable lightshowpi-web.service"
} ## End of systemd installer


## Main
if   [ $(which systemctl) ]; then systemd_install;
elif [ $(which init) ]; then init_install;
else
    echo "Detected neither systemd nor init, not installing web ui service"
    exit 1
fi
