#!/usr/bin/env python
#
# Licensed under the BSD license.  See full license in LICENSE file.
# http://www.lightshowpi.com/
#
# Author: Todd Giles (todd@lightshowpi.com)
# Author: Chris Usey (chris.usey@gmail.com)
# Author: Ryan Jennings
# Author: Paul Dunn (dunnsept@gmail.com)
# Author: Tom Enos (tomslick.ca@gmail.com)
# Author: Stephen Burning

"""Play any audio file and synchronize lights to the music

When executed, this script will play an audio file, as well as turn on
and off N channels of lights to the music (by default the first 8 GPIO
channels on the Rasberry Pi), based upon music it is playing. Many
types of audio files are supported (see decoder.py below), but it has
only been tested with wav and mp3 at the time of this writing.

The timing of the lights turning on and off is based upon the frequency
response of the music being played.  A short segment of the music is
analyzed via FFT to get the frequency response across each defined
channel in the audio range.  Each light channel is then faded in and
out based upon the amplitude of the frequency response in the
corresponding audio channel.  Fading is accomplished with a software
PWM output.  Each channel can also be configured to simply turn on and
off as the frequency response in the corresponding channel crosses a
threshold.

FFT calculation can be CPU intensive and in some cases can adversely
affect playback of songs (especially if attempting to decode the song
as well, as is the case for an mp3).  For this reason, the FFT
cacluations are cached after the first time a new song is played.
The values are cached in a gzip'd text file in the same location as the
song itself.  Subsequent requests to play the same song will use the
cached information and not recompute the FFT, thus reducing CPU
utilization dramatically and allowing for clear music playback of all
audio file types.

Recent optimizations have improved this dramatically and most users are
no longer reporting adverse playback of songs even on the first
playback.

Third party dependencies:

alsaaudio: for audio input/output
    http://pyalsaaudio.sourceforge.net/

decoder.py: decoding mp3, ogg, wma, ...
    https://pypi.python.org/pypi/decoder.py/1.5XB

numpy: for FFT calcuation
    http://www.numpy.org/
"""

# import argparse
import ConfigParser
import audioop
import csv
import fcntl
import json
import logging as log
import os
import random
import subprocess
import sys
import glob
import alsaaudio as aa
import json
import signal
import decoder
import hardware_controller as hc
import numpy as np
import cPickle
import time
import fft
from prepostshow import PrePostShow
import RunningStats

from preshow import Preshow


class Slc:
    def __init__(self):
        self.cm = hc.cm
        self.config = hc.cm.config
        self.port = self.cm.webui.port
        self.music_pipe_r, self.music_pipe_w = os.pipe()
        self.network = hc.network
        self.server = self.network.networking == 'server'
        self.client = self.network.networking == "client"

        self.play_stereo = True
        self.fm_process = None
        self.stream = None
        self.fft_calc = None
        self.output = None

        self.chunk_size = 2048  # Use a multiple of 8 (move this to config)
        self.current_playlist = []
        self.current_song_name = 'none'
        self.audio_chunk = 0
        self.audio_in_mode = False

        self.stream = None
        self.currentlyplaying = None
        self.playlistplaying = None
        self.sr = None
        self.nc = None
        self.fc = None
        self.duration = None
        self.musicfile = None
        self.current_position = None

        # The state of the lights
        # 0 = off
        # 1 = on
        # 2 = playing
        self.lights_state = 0
        self.load_config()

        hc.initialize()

    def load_config(self):
        self.cm = hc.cm
        self.config = hc.cm.config
        self.music_pipe_r, self.music_pipe_w = os.pipe()

        if self.fm_process:
            self.kill_pifm()
            self.fm_process = None

        if self.cm.audio_processing.fm and not self.fm_process:
            self.run_pifm()

        if self.client:
            self.network_client()

        self.set_inits()

    def set_config(self, val):
        # decode the json from the web form into a python dict
        obj = json.loads(val)

        # iterate over that structure entering each option into the config
        for section in obj:
            for keys in obj[section]:
                self.config.set(section, keys, obj[section][keys])

        # write changes to the config file to ~/.lights.cfg
        with open(os.path.expanduser('~/.lights.cfg'), 'wb') as configfile:
            self.config.write(configfile)
            print configfile


    def get_config(self):
        results = '{'
        sections = self.get_sections()
        for i in sections:
            results = results + '"' + i + '":{'
            options = self.get_options(i)
            for j in options:
                if j == 'preshow_configuration':
                    results = results + '"' + j + '":' + self.get(i, j) + ','
                else:
                    results = results + '"' + j + '":"' + self.get(i, j) + '",'
            results = results[:-1] + '},'
        results = results[:-1] + '}'
        return results

    @staticmethod
    def set_config_default():
        # delete the ~/.lights.cfg file so that default configuration is not overridden
        # If using the webui manually changes to the configuration should be made in overrides.cfg
        # leave ~/.lights.cfg for use by the webui; this way
        # this function will delete all changes made by the webui
        os.remove(os.path.expanduser('~/.lights.cfg'))

    def get_sections(self):
        return self.config.sections()

    def get_items(self, section):
        return json.dumps(self.config.items(section))

    def get(self, section, option):
        return self.config.get(section, option)

    def get_options(self, section):
        return self.config.options(section)

    def lightson(self):
        hc.turn_on_lights()
        self.current_song_name = 'none / lights on'
        self.lights_state = 1
        self.set_inits()

    def lighton(self, i):
        hc.turn_on_light(i)
        self.current_song_name = 'none / light(s) on'
        self.set_inits()

    def cleanup(self):
        hc.clean_up()
        self.current_song_name = 'none / lights off'
        self.lights_state = 1
        self.set_inits()

    def lightsoff(self):
        hc.turn_off_lights()
        self.current_song_name = 'none / lights off'
        self.lights_state = 0
        self.set_inits()

    def lightoff(self, i):
        hc.turn_off_light(i)
        self.current_song_name = 'none / light(s) off'
        self.set_inits()

    def after(self):
        if self.cm.lightshow.after_finished:
            hc.turn_on_lights()
            self.current_song_name = 'none / lights of'
            self.lights_state = 1

        else:
            hc.turn_off_lights()
            self.current_song_name = 'none / lights off'
            self.lights_state = 0

    def stop(self):
        self.cm.update_state('play_now', 1)
        self.after()

    def update_lights(self, matrix, mean, std):
        """Update the state of all the lights

        Update the state of all the lights based upon the current
        frequency response matrix

        :param matrix: row of data from cache matrix
        :type matrix: list

        :param mean: standard mean of fft values
        :type mean: list

        :param std: standard deviation of fft values
        :type std: list
        """
        brightness = matrix - mean + (std * 0.5)
        brightness = brightness / (std * 1.25)

        # insure that the brightness levels are in the correct range
        brightness = np.clip(brightness, 0.0, 1.0)
        brightness = np.round(brightness, decimals=3)

        # broadcast to clients if in server mode
        if self.server:
            self.network.broadcast(brightness)

        for blevel, pin in zip(brightness, range(hc.GPIOLEN)):
            hc.set_light(pin, True, blevel)

    def audio_in(self):
        """Control the lightshow from audio coming in from a USB audio card"""
        self.lights_state = 2

        self.current_song_name = 'Audio In Mode'
        sample_rate = self.cm.lightshow.audio_in_sample_rate
        input_channels = self.cm.lightshow.audio_in_channels

        # Open the input stream from default input device
        streaming = aa.PCM(aa.PCM_CAPTURE, aa.PCM_NORMAL, self.cm.lightshow.audio_in_card)
        streaming.setchannels(input_channels)
        streaming.setformat(aa.PCM_FORMAT_S16_LE)  # Expose in config if needed
        streaming.setrate(sample_rate)
        streaming.setperiodsize(self.chunk_size)

        log.debug("Running in audio-in mode")

        mean = np.array([12.0 for _ in range(hc.GPIOLEN)], dtype='float32')
        std = np.array([1.5 for _ in range(hc.GPIOLEN)], dtype='float32')
        count = 2

        running_stats = RunningStats.Stats(hc.GPIOLEN)

        # preload running_stats to avoid errors, and give us a show that looks
        # good right from the start
        running_stats.preload(mean, std, count)
        fft_calc = fft.FFT(self.chunk_size,
                           sample_rate,
                           hc.GPIOLEN,
                           self.cm.audio_processing.min_frequency,
                           self.cm.audio_processing.max_frequency,
                           self.cm.audio_processing.custom_channel_mapping,
                           self.cm.audio_processing.custom_channel_frequencies,
                           input_channels)

        if self.server:
            self.network.network.set_playing()

        # Listen on the audio input device until CTRL-C is pressed
        while self.audio_in_mode:
            length, data = streaming.read()
            if length > 0:
                # if the maximum of the absolute value of all samples in
                # data is below a threshold we will disreguard it
                audio_max = audioop.max(data, 2)
                if audio_max < 250:
                    # we will fill the matrix with zeros and turn the lights off
                    matrix = np.zeros(hc.GPIOLEN, dtype="float32")
                    log.debug("below threshold: '" + str(audio_max) + "', turning the lights off")
                else:
                    matrix = fft_calc.calculate_levels(data)
                    running_stats.push(matrix)
                    mean = running_stats.mean()
                    std = running_stats.std()

                self.update_lights(matrix, mean, std)

        self.after()

        self.set_inits()

    @staticmethod
    def load_custom_config(config_filename):
        """
        Load custom configuration settings for file config_filename

        :param config_filename: string containing path / filename of config
        :type config_filename: str
        """

        """
        example usage
        your song
        carol-of-the-bells.mp3

        First run your playlist (or single files) to create your sync files.  This will
        create a file in the same directory as your music file.
        .carol-of-the-bells.mp3.cfg

        DO NOT EDIT THE existing section [fft], it will cause your sync files to be ignored.

        If you want to use an override you need to add the appropriate section
        The add the options you wish to use, but do not add an option you do not
        want to use, as this will set that option to None and could crash your lightshow.
        Look at defaults.cfg for exact usages of each option

        [custom_lightshow]
        always_on_channels =
        always_off_channels =
        invert_channels =
        preshow_configuration =
        preshow_script =
        postshow_configuration =
        postshow_script =

        [custom_audio_processing]
        min_frequency =
        max_frequency =
        custom_channel_mapping =
        custom_channel_frequencies =

        Note: DO NOT EDIT THE existing section [fft]

        Note: If you use any of the options in "custom_audio_processing" your sync files will be
              automatically regenerated after every change.  This is normal as your sync file needs
              to match these new settings.  After they have been regenerated you will see that they
              now match the settings [fft], and you will not have to regenerate then again.  Unless
              you make more changes again.

        Note: Changes made in "custom_lightshow" do not affect the sync files, so you will not need
              to regenerate them after making changes.
        """
        if os.path.isfile(config_filename):
            config = ConfigParser.RawConfigParser(allow_no_value=True)
            with open(config_filename) as f:
                config.readfp(f)

                if config.has_section('custom_lightshow'):
                    lsc = "custom_lightshow"

                    always_on = "always_on_channels"
                    if config.has_option(lsc, always_on):
                        hc.always_on_channels = map(int, config.get(lsc, always_on).split(","))

                    always_off = "always_off_channels"
                    if config.has_option(lsc, always_off):
                        hc.always_off_channels = map(int, config.get(lsc, always_off).split(","))

                    inverted = "invert_channels"
                    if config.has_option(lsc, inverted):
                        hc.inverted_channels = map(int, config.get(lsc, inverted).split(","))

                    # setup up custom preshow
                    has_preshow_configuration = config.has_option(lsc, 'preshow_configuration')
                    has_preshow_script = config.has_option(lsc, 'preshow_script')

                    if has_preshow_configuration or has_preshow_script:
                        preshow = None
                        try:
                            preshow_configuration = config.get(lsc, 'preshow_configuration')
                        except ConfigParser.NoOptionError:
                            preshow_configuration = None
                        try:
                            preshow_script = config.get(lsc, 'preshow_script')
                        except ConfigParser.NoOptionError:
                            preshow_script = None

                        if preshow_configuration and not preshow_script:
                            try:
                                preshow = json.loads(preshow_configuration)
                            except (ValueError, TypeError) as error:
                                msg = "Preshow_configuration not defined or not in JSON format."
                                log.error(msg + str(error))
                        else:
                            if os.path.isfile(preshow_script):
                                preshow = preshow_script

                        self.cm.lightshow.preshow = preshow

                    # setup postshow
                    has_postshow_configuration = config.has_option(lsc, 'postshow_configuration')
                    has_postshow_script = config.has_option(lsc, 'postshow_script')

                    if has_postshow_configuration or has_postshow_script:
                        postshow = None
                        postshow_configuration = config.get(lsc, 'postshow_configuration')
                        postshow_script = config.get(lsc, 'postshow_script')

                        if postshow_configuration and not postshow_script:
                            try:
                                postshow = json.loads(postshow_configuration)
                            except (ValueError, TypeError) as error:
                                msg = "Postshow_configuration not defined or not in JSON format."
                                log.error(msg + str(error))
                        else:
                            if os.path.isfile(postshow_script):
                                postshow = postshow_script

                        self.cm.lightshow.postshow = postshow

                if config.has_section('custom_audio_processing'):
                    if config.has_option('custom_audio_processing', 'min_frequency'):
                        cm.audio_processing.min_frequency = config.getfloat(
                            'custom_audio_processing', 'min_frequency')

                    if config.has_option('custom_audio_processing', 'max_frequency'):
                        cm.audio_processing.max_frequency = config.getfloat(
                            'custom_audio_processing', 'max_frequency')

                    if config.has_option('custom_audio_processing', 'custom_channel_mapping'):
                        temp = config.get('custom_audio_processing', 'custom_channel_mapping')
                        cm.audio_processing.custom_channel_mapping = map(int, temp.split(
                            ',')) if temp else 0

                    if config.has_option('custom_audio_processing', 'custom_channel_frequencies'):
                        temp = config.get('custom_audio_processing', 'custom_channel_frequencies')
                        cm.audio_processing.custom_channel_frequencies = map(int, temp.split(
                            ',')) if temp else 0

    def play_all(self):
        self.lights_state = 2
        types = ("/home/pi/lightshowpi/music/*.wav",
                 "/home/pi/lightshowpi/music/*.mp1",
                 "/home/pi/lightshowpi/music/*.mp2",
                 "/home/pi/lightshowpi/music/*.mp3",
                 "/home/pi/lightshowpi/music/*.mp4",
                 "/home/pi/lightshowpi/music/*.m4a",
                 "/home/pi/lightshowpi/music/*.m4b",
                 "/home/pi/lightshowpi/music/*.ogg",
                 "/home/pi/lightshowpi/music/*.flac",
                 "/home/pi/lightshowpi/music/*.oga",
                 "/home/pi/lightshowpi/music/*.wma",
                 "/home/pi/lightshowpi/music/*.wmv",
                 "/home/pi/lightshowpi/music/*.aif")

        self.current_playlist = []
        for files in types:
            for m_file in glob.glob(files):
                self.current_playlist.append([os.path.splitext(os.path.basename(m_file))[0],
                                              m_file])

        for song in self.current_playlist:
            if self.lights_state != 2:
                break

            # Get random song
            if self.cm.lightshow.randomize_playlist:
                self.currentlyplaying = \
                    self.current_playlist[random.randint(0, len(self.current_playlist) - 1)][1]
            # Play next song in the lineup
            else:
                self.currentlyplaying = song[1]

            self.playlistplaying = song[0]
            self.play(self.currentlyplaying)

        self.current_playlist = []
        self.after()

    def play_playlist(self, playlist):
        self.lights_state = 2
        most_votes = [None, None, []]
        with open(playlist, 'rb') as playlist_fp:
            fcntl.lockf(playlist_fp, fcntl.LOCK_SH)
            playlist = csv.reader(playlist_fp, delimiter='\t')
            songs = []

            for song in playlist:
                if len(song) < 2 or len(song) > 4:
                    log.error('Invalid playlist.  Each line should be in the form: '
                              '<song name><tab><path to song>')
                    continue
                elif len(song) == 2:
                    song.append(set())
                else:
                    song[2] = set(song[2].split(','))
                    if len(song) == 3 and len(song[2]) >= len(most_votes[2]):
                        most_votes = song

                songs.append(song)
                self.current_playlist.append(song)

            fcntl.lockf(playlist_fp, fcntl.LOCK_UN)

        for song in self.current_playlist:
            if self.lights_state != 2:
                break

            # Get random song
            if self.cm.lightshow.randomize_playlist:
                self.currentlyplaying = \
                    self.current_playlist[random.randint(0, len(self.current_playlist) - 1)][1]
            # Play next song in the lineup
            else:
                self.currentlyplaying = song[1]
            self.playlistplaying = song[0]
            self.play(self.currentlyplaying)
        self.current_playlist = []
        self.after()

    def play_single(self, song):
        self.lights_state = 2

        self.current_playlist = []
        self.current_playlist.append([os.path.splitext(os.path.basename(song))[0], song])
        self.currentlyplaying = self.current_playlist[0][1]
        self.playlistplaying = self.current_playlist[0][0]
        self.play(self.currentlyplaying)
        self.current_playlist = []
        self.after()

    def play(self, m_file):
        self.current_song_name = 'none'
        self.current_position = 0
        self.duration = 0
        play_now = int(self.cm.get_state('play_now', 0))

        log.basicConfig(filename=self.cm.log_dir + '/music_and_lights.play.dbg',
                        format='[%(asctime)s] %(levelname)s {%(pathname)s:%(lineno)d}'
                               ' - %(message)s',
                        level=log.DEBUG)

        # Only execute preshow if no specific song has been requested to be played right now
        if not play_now:
            PrePostShow('preshow', hc).execute()

        # Get filename to play and store the current song playing in state cfg
        song_filename = m_file
        self.current_song_name = os.path.splitext(os.path.basename(song_filename))[0]
        song_filename = song_filename.replace("$SYNCHRONIZED_LIGHTS_HOME", self.cm.home_dir)

        song_filename = os.path.abspath(song_filename)
        config_filename = os.path.dirname(song_filename) + "/." + os.path.basename(
            song_filename) + ".cfg"
        cache_filename = os.path.dirname(song_filename) + "/." + os.path.basename(
            song_filename) + ".sync"

        # load custom configuration from file
        self.load_custom_config(config_filename)

        self.network.set_playing()

        # Ensure play_now is reset before beginning playback
        if play_now:
            self.cm.update_state('play_now', 0)
            play_now = 0

        # Set up audio
        self.setup_audio(song_filename)

        # setup our cache_matrix, std, mean
        cache_found, cache_matrix, std, mean = self.setup_cache(cache_filename)

        # Process audio song_filename
        row = 0
        data = self.musicfile.readframes(self.chunk_size)

        while data != '' and not play_now:
            if self.cm.audio_processing.fm:
                os.write(self.music_pipe_w, data)
            else:
                self.output.write(data)

            self.audio_chunk = data
            self.current_position = self.musicfile.tell() / self.musicfile.getframerate()

            # Control lights with cached timing values if they exist
            matrix = None
            if cache_found:
                if row < len(cache_matrix):
                    matrix = cache_matrix[row]
                else:
                    log.warning("Ran out of cached FFT values, will update the cache.")
                    cache_found = False

            if matrix is None:
                # No cache - Compute FFT in this chunk, and cache results
                matrix = self.fft_calc.calculate_levels(data)
                cache_matrix = np.vstack([cache_matrix, matrix])

            self.update_lights(matrix, mean, std)

            # Read next chunk of data from music song_filename
            data = self.musicfile.readframes(self.chunk_size)
            row += 1

            # Load new application state in case we've been interrupted
            self.cm.load_state()
            play_now = int(self.cm.get_state('play_now', 0))

        if not cache_found:
            self.save_cache(cache_matrix, cache_filename)

        # check for postshow
        self.network.unset_playing()

        if not play_now:
            PrePostShow('postshow', hc).execute()

        self.after()
        self.set_inits()

    def set_inits(self):
        self.musicfile = 0
        self.duration = 0
        self.current_position = 0
        self.playlistplaying = ''

    def run_pifm(self):
        with open(os.devnull, "w") as dev_null:
            self.fm_process = subprocess.Popen(["sudo",
                                                self.cm.home_dir + "/bin/pifm",
                                                "-",
                                                str(self.cm.audio_processing.frequency),
                                                "44100",
                                                "stereo" if self.play_stereo else "mono"],
                                               stdin=self.music_pipe_r,
                                               stdout=dev_null)

    def kill_pifm(self):
        if self.fm_process:
            self.fm_process.kill()

    def setup_audio(self, song_filename):
        """Setup audio file

        and setup setup the output device.output is a lambda that will send data to
        fm process or to the specified ALSA sound card

        :param song_filename: path / filename to music file
        :type song_filename: str
        :return: output, fm_process, fft_calc, music_file
        :rtype tuple: lambda, subprocess, fft.FFT, decoder
        """
        # Set up audio
        force_header = False

        if any([ax for ax in [".mp4", ".m4a", ".m4b"] if ax in song_filename]):
            force_header = True

        self.musicfile = decoder.open(song_filename, force_header)

        sample_rate = self.musicfile.getframerate()
        num_channels = self.musicfile.getnchannels()
        self.sr = sample_rate
        self.nc = num_channels
        self.fc = self.musicfile.getnframes()
        self.duration = self.musicfile.getnframes() / self.musicfile.getframerate()

        self.fft_calc = fft.FFT(self.chunk_size,
                                sample_rate,
                                self.cm.hardware.gpio_len,
                                self.cm.audio_processing.min_frequency,
                                self.cm.audio_processing.max_frequency,
                                self.cm.audio_processing.custom_channel_mapping,
                                self.cm.audio_processing.custom_channel_frequencies)

        self.output = aa.PCM(aa.PCM_PLAYBACK, aa.PCM_NORMAL, self.cm.lightshow.audio_out_card)
        self.output.setchannels(num_channels)
        self.output.setrate(sample_rate)
        self.output.setformat(aa.PCM_FORMAT_S16_LE)
        self.output.setperiodsize(self.chunk_size)

        # Output a bit about what we're about to play to the logs
        nframes = str(self.musicfile.getnframes() / sample_rate)
        log.info("Playing: " + song_filename + " (" + nframes + " sec)")

    def setup_cache(self, cache_filename):
        """Setup the cache_matrix, std and mean

        loading them from a file if it exists, otherwise create empty arrays to be filled

        :param cache_filename: path / filename to cache file
        :type cache_filename: str

        :return:  tuple of cache_found, cache_matrix, std, mean
        :type tuple: (bool, numpy.array, numpy.array, numpy.array)

        :raise IOError:
        """
        # create empty array for the cache_matrix
        cache_matrix = np.empty(shape=[0, hc.GPIOLEN])
        # cache_found = False

        # The values 12 and 1.5 are good estimates for first time playing back
        # (i.e. before we have the actual mean and standard deviations
        # calculated for each channel).
        mean = np.array([12.0 for _ in range(hc.GPIOLEN)], dtype='float32')
        std = np.array([1.5 for _ in range(hc.GPIOLEN)], dtype='float32')

        # Read in cached fft
        try:
            # load cache from file using numpy loadtxt
            cache_matrix = np.loadtxt(cache_filename)

            # compare configuration of cache file to current configuration
            cache_found = self.fft_calc.compare_config(cache_filename)
            if not cache_found:
                # create empty array for the cache_matrix
                cache_matrix = np.empty(shape=[0, hc.GPIOLEN])
                raise IOError()

            # get std from matrix / located at index 0
            std = np.array(cache_matrix[0])

            # get mean from matrix / located at index 1
            mean = np.array(cache_matrix[1])

            # delete mean and std from the array
            cache_matrix = np.delete(cache_matrix, 0, axis=0)
            cache_matrix = np.delete(cache_matrix, 0, axis=0)

            log.debug("std: " + str(std) + ", mean: " + str(mean))
        except IOError:
            cache_found = self.fft_calc.compare_config(cache_filename)
            msg = "Cached sync data song_filename not found: '"
            log.warn(msg + cache_filename + "'.  One will be generated.")

        return cache_found, cache_matrix, std, mean

    def save_cache(self, cache_matrix, cache_filename):
        """
        Save matrix, std, and mean to cache_filename for use during future playback

        :param cache_matrix: numpy array containing the matrix
        :type cache_matrix: numpy.array

        :param cache_filename: name of the cache file to look for
        :type cache_filename: str
        """
        # Compute the standard deviation and mean values for the cache
        mean = np.empty(hc.GPIOLEN, dtype='float32')
        std = np.empty(hc.GPIOLEN, dtype='float32')

        for i in range(0, hc.GPIOLEN):
            std[i] = np.std([item for item in cache_matrix[:, i] if item > 0])
            mean[i] = np.mean([item for item in cache_matrix[:, i] if item > 0])

        # Add mean and std to the top of the cache
        cache_matrix = np.vstack([mean, cache_matrix])
        cache_matrix = np.vstack([std, cache_matrix])

        # Save the cache using numpy savetxt
        np.savetxt(cache_filename, cache_matrix)

        # Save fft config
        self.fft_calc.save_config()

        matrix_len = str(len(cache_matrix))
        log.info("Cached sync data written to '." + cache_filename + "' [" + matrix_len + "rows]")
        log.info("Cached config data written to '." + self.fft_calc.config_filename)

    def network_client(self):
        """Network client support

        If in client mode, ignore everything else and just
        read data from the network and blink the lights
        """
        log.info("Network client mode starting")
        print "Network client mode starting..."
        print "press CTRL<C> to end"

        print

        try:
            channels = self.network.channels
            channel_keys = channels.keys()

            while True:
                data = self.network.receive()

                if isinstance(data[0], int):
                    pin = data[0]
                    if pin in channel_keys:
                        hc.set_light(channels[pin], True, float(data[1]))
                    continue

                elif isinstance(data[0], np.ndarray):
                    blevels = data[0]

                else:
                    continue

                for pin in channel_keys:
                    hc.set_light(channels[pin], True, blevels[pin])

        except KeyboardInterrupt:
            log.info("CTRL<C> pressed, stopping")
            print "stopping"

            self.network.close_connection()
            hc.clean_up()
