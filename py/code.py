#!/usr/bin/env python


import web
import glob
import os
import synchronized_lights as lights
import json
import scheduler_class as scheduler


template_dir = os.path.abspath(os.path.dirname(__file__)) + "/templates"

slc = lights.Slc()
sch = scheduler.scheduler(slc)
env = os.environ['SYNCHRONIZED_LIGHTS_HOME']

urls = (
    '/', 'Index',
    '/index.html', 'Index',
    '/favicon.ico', 'Favicon',
    '/app.manifest', 'Hello',
    '/ajax', 'Ajax',
    '/getvars', 'GetVars',
    '/upload', 'Upload',
    '/scheduler', 'Sched',
    '/music', 'Music',
    '/(js|css|img)/(.*)', 'Static'
)
render = web.template.render(template_dir, cache=True, globals={'glob': glob, 'os': os, 'sch': sch})


class Index(object):

    @staticmethod
    def GET():
        return render.index()


class Hello(object):

    @staticmethod
    def GET():
        web.header('Content-Type', 'text/cache-manifest')
        rmod = "r"

        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + '/py/static/app.manifest', rmod)

        try:
            stream = f.read()
            return stream
        except:
            f.close()
            return '404 Not Found'


class Static(object):

    @staticmethod
    def GET(media, fn):
        rmod = "r"

        if fn.endswith(".png"):
            rmod = "rb"

        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + '/py/static/' + media + '/' + fn, rmod)

        try:
            stream = f.read()
            return stream
        except:
            f.close()
            return '404 Not Found'


class Favicon(object):

    @staticmethod
    def GET():
        f = open(os.environ['SYNCHRONIZED_LIGHTS_HOME'] + "/py/static/favicon.ico", 'rb')
        return f.read()


class Music(object):

    @staticmethod
    def POST():
        web.header('Content-Type', 'application/json')
        return '{"sr":' + str(slc.sr) + ',"nc":' + str(slc.nc) + ',"fc":' + str(slc.fc) + '}'

    @staticmethod
    def GET():
        return slc.thedata


class Ajax(object):

    @staticmethod
    def GET():
        var = web.input()
        if var.option == '0':
            web.header('Content-Type', 'application/json')
            return json.dumps(sch.configData['schedule'])
        elif var.option == '1':
            return slc.audioChunkNumber

    @staticmethod
    def POST():
        varariables = web.input()

        if varariables.option == '0':
            slc.play_playlist(varariables.playlist)

        elif varariables.option == '1':
            slc.play_single(varariables.song)

        elif varariables.option == '2':
            slc.stop()

        elif varariables.option == '3':
            slc.lightson()

        elif varariables.option == '4':
            slc.lightsoff()

        elif varariables.option == 'lightOn':
            # turn on a light
            slc.lighton(int(varariables.port))

        elif varariables.option == 'lightOff':
            # turn off a light
            slc.lightoff(int(varariables.port))

        elif varariables.option == '5':
            web.header('Content-Type', 'application/json')
            return slc.get_config()

        elif varariables.option == '6':
            slc.set_config(varariables.object)

        elif varariables.option == '7':
            slc.play_all()

        elif varariables.option == '8':
            web.header('Content-Type', 'application/json')
            str1 = '{"songs":['

            for f_nane in glob.glob(env + "/music/*.mp3"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            for f_nane in glob.glob(env + "/music/*.wav"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif varariables.option == '9':
            web.header('Content-Type', 'application/json')

            with open(env + "/music/playlists/" + varariables.name + ".playlist", "w") as file_fp:
                file_fp.write(varariables.val)

            str1 = '{"playlists":['

            for f_nane in glob.glob(env + "/music/playlists/*.playlist"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif varariables.option == '10':
            web.header('Content-Type', 'application/json')

            if hasattr(varariables, 'playlist'):
                os.remove(varariables.playlist)

            str1 = '{"playlists":['

            for f_nane in glob.glob(env + "/music/playlists/*.playlist"):
                str1 = str1 + '["' + os.path.basename(f_nane) + '","' + f_nane + '"],'

            str1 = str1[:-1] + ']}'

            return str1

        elif varariables.option == '11':
            slc.set_config_default()

        elif varariables.option == '12':
            app.stop()
            os.system("sudo shutdown -h now")

        elif varariables.option == '13':
            app.stop()
            os.system("sudo shutdown -r now")

        elif varariables.option == '14':
            return slc.audio_chunk

        elif varariables.option == '15':
            web.header('Content-Type', 'application/json')
            return json.dumps(sch.configData['schedule'])

        elif varariables.option == '16':
            if slc.audio_in_mode:
                slc.audio_in_mode = False
            else:
                slc.audio_in_mode = True
                slc.audio_in()


class GetVars(object):

    @staticmethod
    def POST():
        web.header('Content-Type', 'application/json')
        str1 = ''

        for temp in slc.current_playlist:
            str1 = str1 + '"' + temp[0] + '",'

        str1 = str1[:-1]

        return '{"currentsong":"' + slc.current_song_name + '","duration":"' + str(
            slc.duration) + '","currentpos":"' + str(
            slc.current_position) + '","playlist":[' + str1 + '],"playlistplaying":"' +\
               slc.playlistplaying + '","lightstate":"' + str(slc.lights_state) + '"}'

class Upload(object):

    @staticmethod
    def POST():
        # change this to the directory you want to store the file in.
        filedir = env + "/music/"
        i = web.webapi.rawinput()
        files = i.myfile
        if not isinstance(files, list):
            files = [files]

        for x in files:
            # replaces the windows-style slashes with linux ones.
            # filepath = x.filename.replace('\\', '/')
            # splits the and chooses the last part (the filename with extension)
            # filename = filepath.split('/')[-1]
            # creates the file where the uploaded file should be stored

            # Why not this instead of the above? Tested with windows and it works no problem.
            # or is it browser dependent?
            # also we should account for things like spaces in the filename
            f_name = ''.join(os.path.basename(x.filename).split())
            with open(filedir + f_name, 'w') as fout:

                # writes the uploaded file to the newly created file.
                fout.write(x.file.read())

        web.header('Content-Type', 'application/json')
        str1 = '{"songs":['
        for f_name in glob.glob(env + "/music/*.mp3"):
            str1 = str1 + '["' + os.path.basename(f_name) + '","' + f_name + '"],'
        str1 = str1[:-1] + ']}'

        return str1


class Sched(object):

    @staticmethod
    def POST():
        variable = web.input()
        if variable.option == '0':
            return sch.addEvent(variable.type, variable.theif, variable.thethen, variable.arg)
        elif variable.option == '1':
            sch.configData['locationData']['lat'] = variable.lat
            sch.configData['locationData']['lng'] = variable.lng
            sch.saveConfig()
            sch.loadConfig()
        elif variable.option == '2':
            sch.removeEvent(variable.id)


class Application(web.application):
    def run(self, port=8080, *middleware):
        func = self.wsgifunc(*middleware)

        return web.httpserver.runsimple(func, ('0.0.0.0', port))


if __name__ == "__main__":
    web.config.debug = False
    app = Application(urls, globals())
    app.run(port=slc.port)

    sch.stopScheduler()
    slc.lightsoff()    